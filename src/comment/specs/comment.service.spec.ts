import { Test, TestingModule } from '@nestjs/testing';
import { CommentService } from '../comment.service';


class ApiServiceMock {
  getComment(_id: string, _comment: string) {
    return {
      id: "1",
      comment:"lorem ipsum dolor sit amet consectetuer adi piscing elit set diam nonummy",
    };
  }
}

describe('CommentService', () => {
  let service: CommentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CommentService],
      
    }).overrideProvider(CommentService).useValue([{"_id":"1", "title":"testing"}]).compile();

    service = module.get<CommentService>(CommentService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

});
